//21
function stringToArray (str) {
    var a=[]
    for (i=0;i<str.length;i++)
    a.push(str.charAt(i))
    return a
}

console.log(stringToArray("hello"))

//22
function lastN(n){
    let array = ['1','2','3','4','5','6']
    let narray=[];
    if(n==0){
        return null;
    }
    for(let i=array.length - n;i<array.length;i++) 
    {
        narray.push(array[i])
    }
    return narray
}

console.log(lastN(0))

//23
function shiftUnshift(){
    let arr = ['1','2','3','4','5','6']
    console.log(arr.shift()) //returns the removed first element
    console.log(arr.unshift('0')) //returns the length of array after inserting element at start of array.
    console.log(arr)
}
shiftUnshift()

//24
var month_name = function(dt){
    mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    return mlist[dt.getMonth()];
    };
    console.log(month_name(new Date("10/11/2009")));
    
//25
var d = new Date("January 14, 2012");
console.log(d.toLocaleDateString());
d.setMonth(d.getMonth() + 2);
console.log(d.toLocaleDateString());

//26
var dob = new Date("06/24/1998");  
//calculate month difference from current date in time  
var month_diff = Date.now() - dob.getTime();  
//convert the calculated difference in date format  
var age_dt = new Date(month_diff);   

//extract year from date      
var year = age_dt.getUTCFullYear();  
  
//now calculate the age of the user  
var age = Math.abs(year - 1970);  
console.log('Current age is ' + age)

//27
var decimal = 15;
console.log("The decimal number is " + decimal);
var octal = decimal.toString(8);
console.log("The octal number is " + octal);
var hexadecimal = decimal.toString(16);
console.log("The hexadecimal number is " + hexadecimal);
var binary = decimal.toString(2);
console.log("The binary number is " + binary);

//28
var myArray = [
    "Apples",
    "Bananas",
    "Pears"
  ];
  
  var randomItem = myArray[Math.floor(Math.random()*myArray.length)];
  console.log(randomItem)