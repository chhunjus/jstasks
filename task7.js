var str = "ThIs Is A Test";
let upperCount = 0;
let lowerCount = 0;

for(var i = 0; i < str.length; i++)
{
	ch = str.charAt(i);
	if(ch >= 'A' && ch <= 'Z') {
        upperCount++;
    }
    else if (ch >= 'a' && ch <= 'z') {
        lowerCount++;
    }
}

console.log("number of upper case is " + upperCount)
console.log("number of lower case is " + lowerCount)
