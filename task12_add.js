//12
function add(a,b){
    return a+b
}
console.log(add(3,4))


//13
function circle(r){
    var area = 3.14 * r *r 
    var circumference = 2 * 3.14 * r
    console.log('Area of circle is ' + area )
    console.log('Circumference of circle is ' + circumference )
}
circle(4)

//14
function vote(age){
    if (age >= 18) console.log('eligible to vote')
    else console.log('not eligible to vote')
}
vote(28)

//15
function oddEven(num){
    if (num%2==0) console.log('number is even')
    else console.log('number is odd.')
}
oddEven(27)

//16
function range(a,b){
    for(let i=a;i<=b;i++){
        if(i%2!=0){
        console.log(i)
        }
    }

}
range(3,7)

//17
function reverseNum(num){
    let rev = num.toString().split('').reverse().join("")
    console.log(rev)
}
reverseNum(33422)

//18
function returnType(arg){
    return typeof(arg)
}
console.log(returnType(true))
