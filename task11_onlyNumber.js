function onlyNumber(str){
    let ch=""
    for (let i=0;i<str.length;i++){
        ch = str.charAt(i)
        if ((ch >= "a" && ch <= "z") || (ch >= "A" && ch <= "Z")) {
            return false
        }
    }
    return true
}

let word = "1s23"
console.log(onlyNumber(word))