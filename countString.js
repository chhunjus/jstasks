//Question number 2
function countString(word, letter) {
    let count = 0;

    // looping through the items
    for (let i = 0; i < word.length; i++) {

        // check if the character is at that position
        if (word.charAt(i) == letter) {
            count += 1;
        }
    }
    // console.log(count)
    return count;
}

const string = "Sharad"
const letterToCheck = "a";
const result = countString(string, letterToCheck);
console.log(result);
