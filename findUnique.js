//Question number 3
function findUnique(word){
    // The variable that contains the unique values
    let uniq = "";
     
    for(let i = 0; i < word.length; i++){
      // Checking if the uniq contains the character
      if(uniq.includes(word[i]) === false){
        // If the character not present in uniq
        // Concatenate the character with uniq
        uniq += word[i]
      }
    }
    return uniq;
  }
   
  console.log(findUnique("apple"))
