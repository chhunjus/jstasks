function countCharacterType(str) {
    var vowels = 0,
    consonant = 0,
    specialChar = 0,
    digit = 0,
    space =0;


    for (var i = 0; i < str.length; i++) {
    var ch = str[i];

    if ((ch >= "a" && ch <= "z") || (ch >= "A" && ch <= "Z")) {
        ch = ch.toLowerCase();
        if (ch == "a" || ch == "e" || ch == "i" || ch == "o" || ch == "u")
        vowels++;
        else consonant++;
    } 
    else if (ch >= "0" && ch <= "9") digit++;
    else if (ch == " ") space++;
    else specialChar++;
    }
    console.log("Vowels: " + vowels);
    console.log("Consonant: " + consonant);
    console.log("Digit: " + digit );
    console.log("Special Character: " + specialChar );
    console.log("Spaces: " + space );
}

// Driver function.
var str = "This is test123 #Sharad";
countCharacterType(str);

 